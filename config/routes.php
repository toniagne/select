<?php

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::defaultRouteClass(DashedRoute::class);
Router::scope('/', function (RouteBuilder $routes) {

    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true
    ]));  
    $routes->applyMiddleware('csrf');

    //SITE 
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'index']);
    $routes->connect('/beneficios', ['controller' => 'Pages', 'action' => 'advantages']);  
    $routes->connect('/videos', ['controller' => 'Pages', 'action' => 'videos']);  
    $routes->connect('/quem-somos', ['controller' => 'Pages', 'action' => 'company', 'about']); 
    $routes->connect('/saiba-mais', ['controller' => 'Pages', 'action' => 'company', 'about']);  
    $routes->connect('/seja-membro', ['controller' => 'Pages', 'action' => 'company', 'member']); 
    $routes->connect('/news/*', ['controller' => 'Pages', 'action' => 'news']); 
    $routes->connect('/eventos/*', ['controller' => 'Pages', 'action' => 'events']); 
    $routes->connect('/contato', ['controller' => 'Pages', 'action' => 'contact']); 

    //MINHA CONTA
    $routes->connect('/minhaconta/inscricao', ['controller' => 'Users', 'action' => 'subscription']);  
    $routes->connect('/minhaconta/login', ['controller' => 'Users', 'action' => 'login']);  


    $routes->fallbacks(DashedRoute::class);
});

