<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

class PagesController extends AppController
{

    public function beforeRender(\Cake\Event\Event $event)
    {       
        $this->Auth->allow();
    }

    public function index()
    {
          
    }

    public function advantages()
    {

    }

    public function company($segment = null)
    {
       
    }

    public function events($slug = null)
    {
       
    }

    public function videos()
    {

    }

    public function news($slug = null)
    {
       
    }

    public function contact()
    {

    }

}
