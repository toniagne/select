<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

class AppController extends Controller
{

    
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash'); 

      
        $this->loadComponent('Auth', [
            'loginRedirect' => [
                'controller' => 'Account',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Pages',
                'action' => 'index'
            ]
        ]);
        $this->layout = 'site';
    }

    public function beforeFilter(Event $event)
    {
        if ($this->request->param('action') == 'index'):
            $this->set("company_logo", "site/logo-anb-club.png");
        else:
            $this->set("company_logo", "site/logo-anb-club-dark.png");
        endif;
        $this->Auth->allow(['index', 'advantages', 'company', 'news', 'events', 'contact', 'quem-somos']);
    }
}
