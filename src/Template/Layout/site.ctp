<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title><?= Configure::read('Company.name').' | '.Configure::read('Company.slogan'); ?></title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  
<?php
	 echo $this->Html->css('site/wireframe.css'); 
?>
  
</head>
<body>
<header class="fixed-top">
<div class="">
    <div class="container">
      <div class="row mx-3">
		<div class="col-md-3" style="">
		<img class="img-fluid d-block py-2" src="assets\styleguide\logo.jpg" style="">
		</div>
        <div class="text-center text-uppercase col-md-7 my-3" style="">
          <ul class="nav">
            <li class="nav-item">
              <a href="#" class="nav-link active">HOME</a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link active">QUERO ANUNCIAR</a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link active">FALE CONOSCO</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div> 

</header>


<?php echo $this->fetch('content'); ?>


<footer class="footer container-fluid py-5">
<div class="">
    <div class="container">
      <div class="row">
      </div>
      <div class="row">
        <div class="col-md-12">
          <h3 class=""><a href="#"><u>Ajuda e Contato</u></a></h3>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <hr size="1">
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 mt-3">
          <p class="text-left">© Select Catalogos - Brasil</p>
        </div>
      </div>
    </div>
  </div>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
</html>