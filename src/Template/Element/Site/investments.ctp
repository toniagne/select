<section class="investments my-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            <h1 class="mb-4 text-uppercase">Investimento <br>
            </div>
            <div class="col-3">
            <?php
                echo $this->Html->link(
                    $this->Html->image('site/plano-mes.jpg', ["alt" => "AnB Club."]),
                    "/",
                    ['escape' => false, 'class'=>'navbar-brand']
                );
            ?>
            </div>
            <div class="col-3">
            <?php
                echo $this->Html->link(
                    $this->Html->image('site/plano-semestral.jpg', ["alt" => "AnB Club."]),
                    "/",
                    ['escape' => false, 'class'=>'navbar-brand']
                );
            ?>
            </div>
            <div class="col-3">
            <?php
                echo $this->Html->link(
                    $this->Html->image('site/plano-anual.jpg', ["alt" => "AnB Club."]),
                    "/",
                    ['escape' => false, 'class'=>'navbar-brand']
                );
            ?>
            </div>
        </div>
    </div>
</section>