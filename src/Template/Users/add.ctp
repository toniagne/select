
<section class="c-videos bg-escuro">

<h1 class="mb-5 text-white" align="center"><legend><?= __('Add User') ?></legend></h1>
<div class="container">

<div class="users form">
<?= $this->Form->create($user) ?>
    <fieldset>
        
        <?= $this->Form->input('username') ?>
        <?= $this->Form->input('password') ?>
        <?= $this->Form->input('role', [
            'options' => ['admin' => 'Admin', 'author' => 'Author']
        ]) ?>
   </fieldset>
<?= $this->Form->button(__('Submit')); ?>
<?= $this->Form->end() ?>
</div>

        </div>
        </section>