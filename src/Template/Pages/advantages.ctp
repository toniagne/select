<?php

use Cake\Core\Configure;
 
?>
<div class="row py-4"><div class="col-12"></div></div>
<section class="breadcrumbs my-5">
    <div class="container">
        <div class="content-header-left col-md-6 col-12">
        <h2 class="content-header-title mb-0">Benefícios</h2>
    </div>

        <ol class="breadcrumb bg-light m-l-5">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Benefícios</li>
        </ol>
    </div>
</section>

<section class="destaques my-5">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4 col-6 ">
            <?php echo $this->Html->image('site/beneficios-anb-club-home_01.jpg', ['alt' => Configure::read('Company.name'), 'class'=>'img-fluid']); ?>
        </div>
        <div class="col-md-4 col-6 ">
            <?php echo $this->Html->image('site/beneficios-anb-club-home_02.jpg', ['alt' => Configure::read('Company.name'), 'class'=>'img-fluid']); ?>
        </div>
        <div class="col-md-4 col-6 ">
            <?php echo $this->Html->image('site/beneficios-anb-club-home_03.jpg', ['alt' => Configure::read('Company.name'), 'class'=>'img-fluid']); ?>
        </div>
        <div class="col-md-4 col-6 ">
            <?php echo $this->Html->image('site/beneficios-anb-club-home_04.jpg', ['alt' => Configure::read('Company.name'), 'class'=>'img-fluid']); ?>
        </div>
        <div class="col-md-4 col-6 ">
            <?php echo $this->Html->image('site/beneficios-anb-club-home_05.jpg', ['alt' => Configure::read('Company.name'), 'class'=>'img-fluid']); ?>
        </div>
        <div class="col-md-4 col-6 ">
            <?php echo $this->Html->image('site/beneficios-anb-club-home_06.jpg', ['alt' => Configure::read('Company.name'), 'class'=>'img-fluid']); ?>
        </div>
        <div class="col-md-4 my-5">
            <?php
                    echo $this->Html->link(
                    'inscreva-se','/minhaconta/inscricao',
                    ['escape' => false, 'class'=>'btn btn-primary btn-block']
                    );
            ?>
        </div>
    </div>
</div>
</section>

<?= $this->element('Site/investments'); ?>