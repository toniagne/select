<?php 
use Cake\Cache\Cache;
use Cake\Core\Configure; 
 
?> 
<div class="row  py-4"><div class="col-12"></div></div>
<div class="bg-danger" style="">
<div class="container">
  <div class="row">
    <div class="col-md-12 mt-3" style="">
      <div class="form-group"><input type="text" class="form-control form-control-lg w-100 shadow" id="inlineFormInputGroup" style="" placeholder="O que você esta procurando ?"></div>
      <form class="form-inline">
        <div class="input-group">
        </div>
      </form>
    </div>
  </div>
  <div class="row">
    <div class="col-md-1 pl-0 pr-0 text-center" style=""><i class="fa fa-map-marker fa-3x text-dark"></i></div>
    <div class="col-md-6 py-2" style="">
      <h3 class="text-left text-dark"><b>ESCOLHA UM ESTADO</b></h3>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 m-0 ml-4" style=""><a class="btn btn-link text-light m-0 p-1" href="#">SP</a><a class="btn btn-link text-light m-0 p-1" href="#">RJ</a><a class="btn btn-link text-light m-0 p-1" href="#">MG</a><a class="btn btn-link text-light m-0 p-1" href="#">BA</a><a class="btn btn-link text-light m-0 p-1" href="#">ES</a><a class="btn btn-link text-light m-0 p-1" href="#">MT</a><a class="btn btn-link text-light m-0 p-1" href="#">MS</a><a class="btn btn-link text-light m-0 p-1" href="#">RR</a><a class="btn btn-link text-light m-0 p-1" href="#">RS</a><a class="btn btn-link text-light m-0 p-1" href="#">RN</a><a class="btn btn-link text-light m-0 p-1" href="#">TO</a><a class="btn btn-link text-light m-0 p-1" href="#">BRASIL</a></div>
  </div>
  <div class="row text-center">
    <div class="col-md-2 text-center my-3" style="">
      <img class="rounded-circle img-fluid align-items-center" src="assets\styleguide\ic_comercio.png">
      <h6 class="text-light my-2"><b>COMÉRCIO</b></h6>
    </div>
    <div class="col-md-2 text-center my-3" style="">
      <img class="rounded-circle img-fluid align-items-center" src="assets\styleguide\ic_industria.png">
      <h6 class="text-light my-2"><b>INDÚSTRIA</b></h6>
    </div>
    <div class="col-md-2 text-center my-3" style="">
      <img class="rounded-circle img-fluid align-items-center" src="assets\styleguide\ic_servicos.png">
      <h6 class="text-light my-2"><b>COMÉRCIO</b></h6>
    </div>
    <div class="col-md-2 text-center my-3" style="">
      <img class="rounded-circle img-fluid align-items-center" src="assets\styleguide\ic_todas.png">
      <h6 class="text-light my-2"><b>COMÉRCIO</b></h6>
    </div>
    <div class="col-md-3" style=""></div>
  </div>
</div>
</div>
<div class="py-5">
<div class="container">
  <div class="row">
    <div class="col-md-6">
      <h1 style="font-size: 5em; line-height: 0.8;"><b>Aumento&nbsp;<br>suas vendas</b><br></h1>
      <h4 class="text-secondary mt-3"><b>PERFORMANCE 360</b></h4>
      <h4 class="text-secondary">Clique venda online com e sem<br>as tradicionais plataformas de vendas</h4><a class="btn btn-lg btn-dark my-3" href="#" target="_blank">FALAR COM UM CONSULTOR</a>
    </div>
    <div class="col-md-6"><img class="fish ml-5 view overlay" src="assets\styleguide\img_decoracao.png" style="	transform:  scale(0.9); margin-top:-475"></div>
  </div>
</div>
</div>
<div class="py-1">
<div class="container">
  <div class="row">
    <div class="col-md-6"><img class="img-fluid d-block" src="assets\styleguide\img_quemsomos.jpg"></div>
    <div class="col-md-6">
      <h2><b>Quem Somos</b></h2>
      <p class="text-secondary mt-4 lead">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
    </div>
  </div>
</div>
</div>
<div class="pt-5 pb-3 text-center bg-danger">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1 class="mb-3">DESTAQUES</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3 col-6 p-4"> <img class="img-fluid d-block" src="https://static.pingendo.com/img-placeholder-1.svg" width="1500">
    </div>
    <div class="col-md-3 col-6 p-4"> <img class="img-fluid d-block" src="https://static.pingendo.com/img-placeholder-4.svg" width="1500">
    </div>
    <div class="col-md-3 col-6 p-4"> <img class="img-fluid d-block" src="https://static.pingendo.com/img-placeholder-2.svg" width="1500">
    </div>
    <div class="col-md-3 col-6 p-4"> <img class="img-fluid d-block" src="https://static.pingendo.com/img-placeholder-3.svg" width="1500">
    </div>
  </div>
</div>
</div>
<div class="py-5">
<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <h1>PRINCIPAIS CATEGORIAS</h1>
    </div>
  </div>
  <div class="row justify-content-center">
    <div class="p-3 col-md-3" style=""><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a></div>
    <div class="p-3 col-md-3" style=""><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a></div>
    <div class="p-3 col-md-3" style=""><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a></div>
    <div class="p-3 col-md-3" style=""><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a><a class="btn btn-link text-danger" href="#">CATEGORIA 1<br></a></div>
  </div>
</div>
</div>